import json
from datetime import datetime, date, time
import pandas as pd
import seaborn as sns
from matplotlib import pyplot as plt, dates as mdates, patches as mpatches
from config import smogplot as cfg

#_TODO
# add subplots:
# pm1.0, pm10.0; 

if (cfg['paths']['private'] == 'False'):

    # Keep all the data logs here
    datadir = cfg['paths']['datadir']

    # Use this directory for output
    plotdir = cfg['paths']['plotdir']

    # Sensor data log file as found on the root of SD card 
    origfile = cfg['paths']['origfile']

elif (cfg['paths']['private'] == 'True'):
    
    # Keep all the data logs here
    datadir = cfg['paths']['private_datadir']

    # Use this directory for output
    plotdir = cfg['paths']['private_plotdir']

    # Sensor data log file as found on the root of SD card 
    origfile = cfg['paths']['private_origfile']

else: print('Please configure the directories.')

# Orientation of the daily plot (portrait|landscape)
orientation = cfg['output']['orientation']
format = cfg['output']['format']

# Device info
makemodel = cfg['deviceinfo']['make'] + ' ' + cfg['deviceinfo']['model']
# Time settings here should follow time settings on the device

# Starting date of the plots
startday = cfg['time']['startday']

# Day after the last date of the plots
endday = cfg['time']['endday']

# Daily starting time
fromoclock = cfg['time']['fromoclock']

# Daily ending time
tilloclock = cfg['time']['tilloclock']

# Functional plot background colors / alphas
col_g = cfg['colors']['good']
col_m = cfg['colors']['moderate']
col_u = cfg['colors']['unhealthy']
col_h = cfg['colors']['hazardous']
alpha_g = cfg['alphas']['good']
alpha_m = cfg['alphas']['moderate']
alpha_u = cfg['alphas']['unhealthy']
alpha_h = cfg['alphas']['hazardous']

# Misc constants
hazmult = cfg['misc_constants']['hazmult']

# Location of measurements
where = cfg['location']['name']

# Language related
if (cfg['output']['language'] == 'en'):
    names = cfg['labelnames_en']
    descriptions = cfg['descriptions_en']
elif (cfg['output']['language'] == 'de'):
    names = cfg['labelnames_de']
    descriptions = cfg['descriptions_de']

def main():

    # Open input file and read lines
    with open(origfile, 'r') as input_file:
        lines = input_file.readlines()

    # Parse log entries and create list of dicts. This script uses only some of all the columns.
    # Note the availability of extra data such as PM10.0 (important with diesel emmission measurements)
    # or numerical dust particle measurement data columns. Use these as appropriate for your reports
    data = []
    for line in lines:
        fields = line.strip().split()
        entry = {}
        for field in fields:
            key, value = field.split(':')
            if key.startswith('Date'):
                entry['datetime'] = value[:4] + '-' + value[4:6] + '-' + value[6:]
            elif key.startswith('Time'):
                entry['datetime'] = entry['datetime'] + 'T' + value[:2] + ':' + value[2:] + '+00:00'
            elif key.startswith('PM1.0'):
                entry['pm1.0'] = int(value)
            elif key.startswith('PM2.5'):
                entry['pm2.5'] = int(value)
            elif key.startswith('PM10'):
                entry['pm10'] = int(value)
            elif key.startswith('CO2'):
                entry['co2'] = int(value)
            elif key.startswith('HCHO'):
                entry['hcho'] = float(value)
            elif key.startswith('TVOC'):
                entry['tvoc'] = float(value)
            elif key.startswith('>0.3DustNum'):
                entry['>0.3dnum'] = int(value)
            elif key.startswith('>0.5DustNum'):
                entry['>0.5dnum'] = int(value)
            elif key.startswith('>1.0DustNum'):
                entry['>1.0dnum'] = int(value)
            elif key.startswith('2.5DustNum'):
                entry['>2.5dnum'] = int(value)
            elif key.startswith('>5.0DustNum'):
                entry['>5.0dnum'] = int(value)
            elif key.startswith('>10DustNum'):
                entry['>10dnum'] = int(value)
        data.append(entry)

    # Write an intermediary JSON file.
    with open(datadir + '/' + startday + '_' + endday, 'w') as output_file:
        json.dump(data, output_file)
       
    # For finetuning output on a fresh existing intermediary file, place multiline
    # comments from line before original file access to line before start of this comment

    # Load JSON data
    with open(datadir + '/' + startday + '_' + endday, 'r') as f:
        data = json.load(f)

    # Convert JSON data to pandas DataFrame
    df = pd.DataFrame(data)

    # Set start(inclusive) and end(exclusive) dates
    df['datetime'] = pd.to_datetime(df['datetime'])
    sdate = pd.to_datetime(startday + ' 00:00 +0000')
    edate = pd.to_datetime(endday + ' 00:00 +0000')

    # Set daily start and end times (inclusive)
    stime = fromoclock 
    etime = tilloclock 

    df = df[(df['datetime'] > sdate) & (df['datetime'] < edate)]

    # Levels of danger and respective units for different hazardous substances.
    rangesteps_pm25 = [0, 12, 34, 150]
    rangesteps_co2 = [0, 700, 1500, 2000]
    rangesteps_hcho = [0.0, 0.1, 0.15, 0.2]
    rangesteps_tvoc = [0.0, 0.5, 1.0, 1.5]

    hazardcols = ['pm2.5', 'co2', 'hcho', 'tvoc']
    ylabelnames = ['PM2.5 (μg/m3)', 'CO2 (ppm)', 'HCHO (mg/m3)', 'TVOC (mg/m3)']
    xlabelname = names['xlabelname']

    if (cfg['output']['orientation'] == 'landscape'):
        figsize = (11.69,8.27) 
    elif (cfg['output']['orientation'] == 'portrait'):
        figsize = (8.27,11.69) 

    for date, subframe in df.groupby(pd.Grouper(key='datetime', freq='D')):

        subframe['time_of_day'] = pd.to_datetime(str(date) + ' ' + subframe['datetime'].dt.time.astype(str))
         
        fig, axes = plt.subplots(nrows=4, ncols=1, sharex=False, figsize=figsize)

        legend_good = mpatches.Patch(color = col_g, alpha = alpha_g, label=names['good'])
        legend_moderate = mpatches.Patch(color = col_m, alpha = alpha_m, label=names['moderate'])
        legend_unhealthy = mpatches.Patch(color = col_u, alpha = alpha_u, label=names['unhealthy'])
        legend_hazardous = mpatches.Patch(color = col_h, alpha = alpha_h, label=names['hazardous'])

        plt.legend(\
        bbox_to_anchor=(1, 5.25), loc="upper right",\
        handles=[legend_hazardous,legend_unhealthy, legend_moderate, legend_good],\
        framealpha=1.0\
        )

        rs = rangesteps_pm25
        index = 0
        axes[index].set_xlim(left=pd.Timestamp(f"{date.strftime('%Y-%m-%d')}" + ' ' + stime),\
        right=pd.Timestamp(f"{date.strftime('%Y-%m-%d')}" + ' ' + etime))
        sns.lineplot(x="time_of_day", y=hazardcols[index], data=subframe, ax=axes[index], errorbar=None)
        axes[index].set_title(f"{names['titlename']}" + '\n' +\
        names['date'] + ': ' + f"{date.strftime('%Y-%m-%d')}" + '\n' +\
        names['timeframe'] + ': ' + fromoclock + ' - ' + tilloclock + '\n' +\
        f"{where}" + '\n' +\
        names['hardware'] + ': ' + makemodel, fontsize=10, loc='left', horizontalalignment='left')
        axes[index].set_ylabel(ylabelnames[index] + '\n' + descriptions['pm25'], fontsize=8)
        axes[index].xaxis.set_major_locator(mdates.HourLocator(interval=1))
        axes[index].tick_params(labelbottom=False)
        axes[index].set(xlabel=None)
        
        good = mpatches.Rectangle(
            xy = (axes[index].get_xlim()[0], rs[0]),
            width = axes[index].get_xlim()[1] - axes[index].get_xlim()[0],
            height = rs[1],
            color = col_g, alpha = alpha_g
        )
        axes[index].add_patch(good)

        moderate = mpatches.Rectangle(
            xy = (axes[index].get_xlim()[0], rs[1]),
            width = axes[index].get_xlim()[1] - axes[index].get_xlim()[0],
            height = rs[2] - rs[1],
            color = col_m, alpha = alpha_m
        )
        axes[index].add_patch(moderate)

        unhealthy = mpatches.Rectangle(
            xy = (axes[index].get_xlim()[0], rs[2]),
            width = axes[index].get_xlim()[1] - axes[index].get_xlim()[0],
            height = rs[3] - rs[2],
            color = col_u, alpha = alpha_u
        )
        axes[index].add_patch(unhealthy)

        hazardous = mpatches.Rectangle(
            xy=(axes[0].get_xlim()[0], rs[3]),
            width = axes[index].get_xlim()[1] - axes[index].get_xlim()[0],
            height = rs[3] * hazmult,
            color = col_h, alpha = alpha_h
        )
        axes[index].add_patch(hazardous)

        rs = rangesteps_co2
        index = 1
        axes[index].set_xlim(left=pd.Timestamp(f"{date.strftime('%Y-%m-%d')}" + ' ' + stime),\
        right=pd.Timestamp(f"{date.strftime('%Y-%m-%d')}" + ' ' + etime))
        sns.lineplot(x="time_of_day", y=hazardcols[index], data=subframe, ax=axes[index], errorbar=None)
        axes[index].set_ylabel(ylabelnames[index] + '\n' + descriptions['co2'], fontsize=8)
        axes[index].xaxis.set_major_locator(mdates.HourLocator(interval=1))
        axes[index].tick_params(labelbottom=False)
        axes[index].set(xlabel=None)
        
        good = mpatches.Rectangle(
            xy = (axes[index].get_xlim()[0], rs[0]),
            width = axes[index].get_xlim()[1] - axes[index].get_xlim()[0],
            height = rs[1],
            color = col_g, alpha = alpha_g
        )
        axes[index].add_patch(good)

        moderate = mpatches.Rectangle(
            xy = (axes[index].get_xlim()[0], rs[1]),
            width = axes[index].get_xlim()[1] - axes[index].get_xlim()[0],
            height = rs[2] - rs[1],
            color = col_m, alpha = alpha_m
        )
        axes[index].add_patch(moderate)

        unhealthy = mpatches.Rectangle(
            xy = (axes[index].get_xlim()[0], rs[2]),
            width = axes[index].get_xlim()[1] - axes[index].get_xlim()[0],
            height = rs[3] - rs[2],
            color = col_u, alpha = alpha_u
        )
        axes[index].add_patch(unhealthy)

        hazardous = mpatches.Rectangle(
            xy=(axes[index].get_xlim()[0], rs[3]),
            width=axes[index].get_xlim()[1] - axes[index].get_xlim()[0],
            height = rs[3] * hazmult,
            color = col_h, alpha = alpha_h
        )
        axes[index].add_patch(hazardous)

        rs = rangesteps_hcho
        index = 2

        axes[index].set_xlim(left=pd.Timestamp(f"{date.strftime('%Y-%m-%d')}" + ' ' + stime),\
        right=pd.Timestamp(f"{date.strftime('%Y-%m-%d')}" + ' ' + etime))
        sns.lineplot(x="time_of_day", y=hazardcols[index], data=subframe, ax=axes[index], errorbar=None)
        axes[index].set_ylabel(ylabelnames[index] + '\n' + descriptions['hcho'], fontsize=8)
        axes[index].xaxis.set_major_locator(mdates.HourLocator(interval=1))
        axes[index].tick_params(labelbottom=False)
        axes[index].set(xlabel=None)
        
        good = mpatches.Rectangle(
            xy = (axes[index].get_xlim()[0], rs[0]),
            width = axes[index].get_xlim()[1] - axes[index].get_xlim()[0],
            height = rs[1],
            color = col_g, alpha = alpha_g
        )
        axes[index].add_patch(good)
        
        moderate = mpatches.Rectangle(
            xy = (axes[index].get_xlim()[0], rs[1]),
            width = axes[index].get_xlim()[1] - axes[index].get_xlim()[0],
            height = rs[2] - rs[1],
            color = col_m, alpha = alpha_m
        )
        axes[index].add_patch(moderate)

        unhealthy = mpatches.Rectangle(
            xy = (axes[index].get_xlim()[0], rs[2]),
            width = axes[index].get_xlim()[1] - axes[index].get_xlim()[0],
            height = rs[3] - rs[2],
            color = col_u, alpha = alpha_u
        )
        axes[index].add_patch(unhealthy)

        hazardous = mpatches.Rectangle(
            xy=(axes[index].get_xlim()[0], rs[3]),
            width=axes[index].get_xlim()[1] - axes[index].get_xlim()[0],
            height = rs[3] * hazmult,
            color = col_h, alpha = alpha_h
        )
        axes[index].add_patch(hazardous)

        rs = rangesteps_tvoc
        index = 3

        axes[index].set_xlim(left=pd.Timestamp(f"{date.strftime('%Y-%m-%d')}" + ' ' + stime),\
        right=pd.Timestamp(f"{date.strftime('%Y-%m-%d')}" + ' ' + etime))
        sns.lineplot(x="time_of_day", y=hazardcols[index], data=subframe, ax=axes[index], errorbar=None)
        axes[index].set_ylabel(ylabelnames[index] + '\n' + descriptions['tvoc'], fontsize=8)
        axes[index].set_xlabel(xlabelname, fontsize=10)
        axes[index].tick_params(axis='x', rotation=30)
        axes[index].xaxis.set_major_locator(mdates.HourLocator(interval=1))
        axes[index].xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))

        good = mpatches.Rectangle(
            xy = (axes[index].get_xlim()[0], rs[0]),
            width = axes[index].get_xlim()[1] - axes[index].get_xlim()[0],
            height = rs[1],
            color = col_g, alpha = alpha_g
        )
        axes[index].add_patch(good)
        
        moderate = mpatches.Rectangle(
            xy = (axes[index].get_xlim()[0], rs[1]),
            width = axes[index].get_xlim()[1] - axes[index].get_xlim()[0],
            height = rs[2] - rs[1],
            color = col_m, alpha = alpha_m
        )
        axes[index].add_patch(moderate)

        unhealthy = mpatches.Rectangle(
            xy = (axes[index].get_xlim()[0], rs[2]),
            width = axes[index].get_xlim()[1] - axes[index].get_xlim()[0],
            height = rs[3] - rs[2],
            color = col_u, alpha = alpha_u
        )
        axes[index].add_patch(unhealthy)

        hazardous = mpatches.Rectangle(
            xy=(axes[index].get_xlim()[0], rs[3]),
            width=axes[index].get_xlim()[1] - axes[index].get_xlim()[0],
            height = rs[3] * hazmult,
            color = col_h, alpha = alpha_h
        )
        axes[index].add_patch(hazardous)

        fig.align_ylabels(axes[:])
        plt.xticks(fontsize=8)

        if (cfg['output']['format'] == 'show'): 
            plt.show()
        else:
            y = str(date.year)
            m = str(date.month)
            d = str(date.day)
            
            filename = plotdir + '/' + y + '-' + m.zfill(2) + '-' + d.zfill(2) + '.' + format
            plt.savefig(filename, format=format)
        
if __name__ == "__main__":
    main()