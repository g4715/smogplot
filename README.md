# SmogPlot

![cropped screenshot](./reports/plot.png)
## What it is:

smogplot is an air pollution visualization script written around matplotlib/seaborn library. 

## Why it exists:

Purpose of this is to prepare comprehensible air pollution line plots. Currently it will accept logfile of a certain format that is found on such device as [BLATN BR-Smart 128s](https://blatn.com/products/indoor-air-quality-monitor-br-smart-128s-co2-pm2-5-tvoc-hcho). It should be trivial to adapt/refactor this script for use with other possible air pollution data log formats. 

## Hardware involved

The above mentioned portable indoor air pollution monitor can constantly measure several air
pollutants simultaneously (such as [PM2.5](https://ww2.arb.ca.gov/resources/inhalable-particulate-matter-and-health), [CO2](https://en.wikipedia.org/wiki/Carbon_dioxide), [HCHO](https://en.wikipedia.org/wiki/Formaldehyde) and [TVOC](https://en.wikipedia.org/wiki/Volatile_organic_compound#Indoor_VOCs)) and will save
every second of the data into a text file onto an SD card.

## What it does:

The plot layout used in this script is loosely inspired by one of the display modes found on the above mentioned device.

By default smogplot will produce an A4 format plot set in portrait orientation with four above mentioned pollutant axes as Y's and a common time of day X axis.

## How to use

All configuration is done via ```smogplot.toml``` file found in the ```./config``` directory. You can find more detailed info in source and configuration file comments.

First, copy the original logfile named ```Save.TXT``` found in the root of the included SD card into the ```./data``` subdirectory. Inside ```smogplot.toml``` file edit ```startday``` and ```endday``` values along with desired daily time windows. Choose output format such as ```pdf``` or ```png```, or simply ```show``` to open a plot window.
Call the script with ```python smogplot.py```. Pay attention to ```./private``` subdirectory settings in ```smogplot.toml``` file.

```
git clone https://codeberg.org/g4715/smogplot.git
cd smogplot
python -m venv env
source env/bin/activate
pip install -r requirements.txt
```
Customize variables (including date and time ranges) in the ./config/smogplot.toml
```
python smogplot.py
```

## N.B.
There is no smoothing applied to the plots. The purpose of this is to provide 1:1 relationship with datapoints on the SD card.